#!/usr/bin/env tarantool
box.cfg{ wal_mode = 'none' }
local orm = require 'orm' { create_if_not_exists = true }
_G.orm = orm

orm:configure{
	users = {
		format = {
			{ name = 'id',      type = 'uuid',     auto = 'uuid' },
			{ name = 'created', type = 'unsigned', auto = 'time' },
		},

		primary = {'id'}
	},

	tasks = {
		format = {
			{ name = 'id',       type = 'unsigned', auto = 'time64'     },
			{ name = 'dedup',    type = 'string',   is_nullable = true  },
			{ name = 'status',   type = 'string',                       },
			{ name = 'runat',    type = 'unsigned',                     },
			{ name = 'group',    type = 'string',                       },
			{ name = 'tube',     type = 'string',                       },
			{ name = 'pri',      type = 'unsigned',                     },
			{ name = 'mtime',    type = 'unsigned',                     },
			{ name = 'attempts', type = 'unsigned',                     },
			{ name = 'payload',  type = 'any'                           },
			{ name = 'error',    type = 'any',      is_nullable = true  },
		},

		primary = {'id'},
		secondaries = {
			{ name = 'take',  parts = {'status', 'pri', 'id'} },
			{ name = 'tube',  parts = {'tube', 'status', 'pri', 'id'} },
			{ name = 'group', parts = {'group', 'id'} },
			{ name = 'runat', parts = {'runat', 'id'} },
			{ name = 'dedup', parts = {'dedup'} },
		},
	},
}