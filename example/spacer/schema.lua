local ddl = require 'orm' {
	spacer = box.spacer,
	automigrate = true,
}

ddl:configure{
	users = {
		format = {
			{ name = 'uuid',  type = 'uuid'     },
			{ name = 'email', type = 'string'   },
			{ name = 'ctime', type = 'unsigned' },
		},
		primary = {'uuid'},
		secondaries = {
			{ name = 'email', parts = {'email'} },
		},
	},
	tickets = {
		format = {
			{ name = 'uuid',  type = 'uuid' },
			{ name = 'user',  type = 'uuid', ref = 'users' },
			{ name = 'email', type = 'string' },
			{ name = 'event', type = 'uuid', ref = 'events' },
			{ name = 'ctime', type = 'unsigned' },
			{ name = 'meta',  type = '*' },
		},
		primary = { 'uuid' },
		secondaries = {
			{ name = 'email', parts = { 'email', 'uuid' }  },
			{ name = 'user',  parts = { 'user', 'uuid' }   },
		},
	},
	events = {
		format = {
			{ name = 'uuid', type = 'uuid' },
			{ name = 'time', type = 'unsigned' },
			{ name = 'meta', type = '*' },
		},
		primary = { 'uuid' },
	},
}

ddl.spacer = nil

return ddl
