#!/usr/bin/env tarantool
box.cfg{wal_mode='none'}
box.spacer = require 'spacer'.new {
	migrations = "example/spacer/migrations",
}

rawset(_G, 'orm', require 'example.spacer.schema')

require 'console'.start()