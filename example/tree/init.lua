box.cfg{ wal_mode = 'none' }
local orm = require 'orm' { create_if_not_exists = true }
rawset(_G, 'orm', orm)

orm:configure {
	nodes = {
		circular = true,

		format = {
			{ name = 'id',     type = 'unsigned' },
			{ name = 'parent', type = 'unsigned', ref = 'nodes' },
		},

		primary = {'id'},
		secondaries = {
			{ name = 'parent', parts = {'parent', 'id'} },
		}
	},
}