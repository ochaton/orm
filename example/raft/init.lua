#!/usr/bin/env tarantool
box.cfg{ wal_mode = 'none' }
local orm = require 'orm' { create_if_not_exists = true }
_G.orm = orm

orm:configure{
	terms = {
		format = {
			{ name = 'id',     type = 'unsigned' },
			{ name = 'leader', type = 'unsigned', ref = 'members', is_nullable = true },
		},

		primary = {'id'}
	},
	members = {
		format = {
			{ name = 'id',     type = 'unsigned' },
		},
		primary = {'id'}
	},
	votes = {
		format = {
			{ name = 'term',   type = 'unsigned', ref = 'terms'   },
			{ name = 'member', type = 'unsigned', ref = 'members' },
			{ name = 'vote',   type = 'unsigned', ref = 'members' },
		},

		primary = {'term', 'member'},
		secondaries = {
			{ name = 'member', parts = {'member', 'term'} },
		},
	},
}