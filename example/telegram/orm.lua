local orm = require 'orm':new()
package.loaded[...] = orm

orm:configure {
	users = {
		class = require 'example.user',
		format = {
			{ name = 'id',            type = 'number'  },
			{ name = 'is_bot',        type = 'boolean' },
			{ name = 'first_name',    type = 'string'  },
			{ name = 'last_name',     type = 'string',  is_nullable = true },
			{ name = 'username',      type = 'string',  is_nullable = true },
			{ name = 'language_code', type = 'string',  is_nullable = true },
		},
		primary = {'id'},
	}
}

return orm