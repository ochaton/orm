local class = require 'metaclass'
local orm = require 'example.orm'


local User = class "User" : inherits(require "orm.Tuple")

function User:greet()
	return ("Hello, %s"):format(self.nickname)
end

function User:friends()
	return orm.space.friends.index.user:pairs(self):totable()
end

return User