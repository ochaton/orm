local ddl = require 'orm' {
	create_if_not_exists = true,
}
package.loaded[...] = ddl

ddl:configure{
	vets = {
		format = {
			{ name = 'id',         type = 'unsigned' },
			{ name = 'first_name', type = 'string', is_nullable = true },
			{ name = 'last_name',  type = 'string', is_nullable = true },
		},

		primary = { 'id' },
	},

	specialities = {
		format = {
			{ name = 'id',   type = 'unsigned' },
			{ name = 'name', type = 'string', is_nullable = true },
		},

		primary = { 'id' },
	},

	vet_specialities = {
		format = {
			{ name = 'vet',        type = 'unsigned', ref = 'vets' },
			{ name = 'speciality', type = 'unsigned', ref = 'specialities' },
		},

		primary = { 'vet', 'speciality' },
	},

	users = {
		format = {
			{ name = 'username', type = 'string' },
			{ name = 'password', type = 'string' },
			{ name = 'enabled',  type = 'boolean' },
		},

		primary = { 'username' },
	},

	roles = {
		format = {
			{ name = 'id',       type = 'unsigned' },
			{ name = 'username', type = 'string', ref = 'users' },
			{ name = 'role',     type = 'string' },
		},

		primary = {'id'},
	},

	visits = {
		format = {
			{ name = 'id',          type = 'unsigned' },
			{ name = 'pet',         type = 'unsigned', ref = 'pets' },
			{ name = 'visit_date',  type = 'unsigned', is_nullable = true },
			{ name = 'description', type = 'string', is_nullable = true },
		},

		primary = { 'id' },
		secondaries = {
			{ name = 'pet', parts = {'pet', 'id'} },
		},
	},

	pets = {
		format = {
			{ name = 'id', type = 'unsigned' },
			{ name = 'name', type = 'string' },
			{ name = 'birth_date', type = 'unsigned', is_nullable = true },
			{ name = 'type',  type = 'unsigned', ref = 'types' },
			{ name = 'owner', type = 'unsigned', ref = 'owners' },
		},

		primary = { 'id' },
		secondaries = {
			{ name = 'owner', parts = {'owner', 'id'} },
		},
	},

	owners = {
		format = {
			{ name = 'id',         type = 'unsigned' },
			{ name = 'first_name', type = 'string', is_nullable = true },
			{ name = 'last_name',  type = 'string', is_nullable = true },
			{ name = 'address',    type = 'string', is_nullable = true },
			{ name = 'city',       type = 'string', is_nullable = true },
			{ name = 'telephone',  type = 'string', is_nullable = true },
		},
		primary = {'id'},
	},

	types = {
		format = {
			{ name = 'id', type = 'unsigned' },
			{ name = 'name', type = 'string', is_nullable = true },
		},

		primary = {'id'},
		secondaries = {
			{ name = 'name', parts = {'name', 'id'} },
		}
	},
}

return ddl