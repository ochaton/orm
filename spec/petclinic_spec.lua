require 'jit'.off()
local log = require 'log'
local fio = require 'fio'

local tmpdir = fio.pathjoin(
	fio.dirname(fio.abspath(debug.getinfo(1, "S").source:match("^@(.+)$"))),
	'..',
	'tmp',
	'data'
)

log.info("tmpdir: %s", tmpdir)

setup(function()
	log.info("Global setup: %s", tmpdir)
	fio.rmtree(tmpdir)
	fio.mktree(tmpdir)
	box.cfg{ memtx_dir = tmpdir, wal_dir = tmpdir, log_level = 5 }
end)

describe("Tests CRUD API", function()
	local orm
	setup(function()
		log.info "Running setup"
		orm = require 'example.petclinic.init'
		for _, space in pairs(orm.space) do space.space:truncate() end
	end)

	describe("pets", function()
		setup(function()
			orm.space.owners:insert {
				id = 1,
				first_name = 'Eduardo',
				last_name = 'Rodriquez',
				address = '2693 Commerce St.',
				city = 'McFarland',
				telephone = '6085558763',
			}
			orm.space.types:insert {
				id = 2,
				name = 'dog',
			}
		end)

		before_each(function()
			local rosy = orm.space.pets:replace {
				id = 3,
				name = 'Rosy',
				birth_date = os.time(),
				owner = orm.space.owners:get(1),
				type = 2,
			}

			orm.space.pets:replace {
				id = 4,
				name = 'Jewel',
				birth_date = os.time(),
				owner = rosy.owner,
				type = rosy.type,
			}
		end)

		teardown(function()
			for _, space in pairs(orm.space) do
				space.space:truncate()
			end
		end)

		it("should get rosy", function()
			local pet = orm.space.pets:get(3)
			assert.is_equal(pet.id, 3)
			assert.is_equal(pet.name, 'Rosy')
		end)

		it("should return nothing", function()
			assert.is_nil(orm.space.pets:get(2ULL^64-1))
		end)

		it("should return all pets", function()
			local pets = orm.space.pets:select()
			assert.is_equal(pets[1].id, 3)
			assert.is_equal(pets[1].name, 'Rosy')
			assert.is_equal(pets[2].id, 4)
			assert.is_equal(pets[2].name, 'Jewel')
		end)

		it("should not find any pet", function()
			orm.space.pets.space:truncate()
			assert.are.same(0, #orm.space.pets:select())
		end)

		it("should create new pet", function()
			local pet = orm.space.pets:insert {
				id = 999,
				name = 'Fluffy',
				type = orm.space.types.index.name:pairs('dog'):nth(1),
				owner = orm.space.owners:pairs():nth(1),
			}

			assert.is_not_nil(pet)
		end)

		it("should fail to create pet", function()
			assert.has.errors(function()
				orm.space.pets:insert{
					id = box.NULL, name = box.NULL,
				}
			end)
		end)

		it("should be ok to update pet", function()
			local rosy = orm.space.pets:get(3)
				:update{ name = 'Rosy I' }

			assert.is.equal(rosy.id, 3)
			assert.is.equal(rosy.name, 'Rosy I')

			rosy = orm.space.pets:get(3)
			assert.is.equal(rosy.id, 3)
			assert.is.equal(rosy.name, 'Rosy I')
		end)

		it("should fail to update pet", function()
			assert.has.errors(function()
				orm.space.pets:get(3):update{ name = box.NULL }
			end)
		end)

		it("should delete pet", function()
			local rosy = orm.space.pets:delete(3)
			assert.is.equal(rosy.id, 3)
			assert.is.equal(rosy.name, 'Rosy')
			assert.is_nil(orm.space.pets:get(3))
		end)

		it("should fail to delete pet", function()
			assert.has.errors(function()
				orm.space.pets:delete(-1)
			end)
		end)
	end)

	describe("users", function()
		it("should create user", function()
			local user = orm.space.users.class {
				username = 'username',
				password = 'password',
				enabled  = true,
				_space = orm.space.users,
			}
			local role = orm.space.roles.class {
				id = 1,
				role = 'OWNER_ADMIN',
				username = user,
				_space = orm.space.roles,
			}

			box.atomic(function() role:flush() end)
			assert.is_equal(orm.space.users:get("username").username, "username")
		end)

		it("should fail to create user", function()
			assert.has.errors(function()
				orm.space.users:insert {
					username = 'username',
					password = 'password',
					enabled  = true,
				}
			end)
		end)
	end)

	describe("visits", function()
		setup(function()
			local owner = orm.space.owners:replace{
				id = 1,
				first_name = 'Eduardo',
				last_name = 'Rodriquez',
				address = '2693 Commerce St.',
				city = 'McFarland',
				telephone = '6085558763',
			}

			local pettype = orm.space.types:replace {
				id = 2,
				name = 'dog',
			}

			local pet = orm.space.pets:replace {
				id = 8,
				name = 'Rosy',
				birth_date = os.time(),
				owner = owner,
				type = pettype,
			}

			orm.space.visits:replace {
				id = 2,
				pet = pet,
				date = os.time(),
				description = 'rabies shot',
			}

			orm.space.visits:replace {
				id = 3,
				pet = pet,
				date = os.time(),
				description = 'neutered',
			}
		end)

		it("should get visit", function()
			local visit = orm.space.visits:get(2)
			assert.is.equal(2, visit.id)
			assert.is.equal('rabies shot', visit.description)
		end)

		it("should not found visit", function()
			assert.is_nil(orm.space.visits:get(2^31))
		end)

		it("should return all visits", function()
			local visits = orm.space.visits.index.pet:select(
				orm.space.pets.index.owner:pairs(
					orm.space.owners:get(1)
				):nth(1)
			)

			assert.is_equal(#visits, 2)
			assert.is_equal(visits[1].id, 2)
			assert.is_equal(visits[1].description, 'rabies shot')
			assert.is_equal(visits[2].id, 3)
			assert.is_equal(visits[2].description, 'neutered')
		end)
	end)
end)

describe("false circular", function()
	local orm
	setup(function()
		orm = require 'orm' {}
		orm:configure {
			father = {
				format = {
					{ name = 'id', type = 'unsigned' },
				},
				primary = {'id'},
			},
			mother = {
				format = {
					{ name = 'id',   type = 'unsigned' },
				},
				primary = {'id'}
			},
			son = {
				format = {
					{ name = 'id', type = 'unsigned' },
					{ name = 'mother', type = 'unsigned', ref = 'mother' },
					{ name = 'father', type = 'unsigned', ref = 'father' },
				},
				primary = {'id'}
			},
			daughter = {
				format = {
					{ name = 'id', type = 'unsigned' },
					{ name = 'mother', type = 'unsigned', ref = 'mother' },
					{ name = 'father', type = 'unsigned', ref = 'father' },
				},
				primary = {'id'}
			},
		}
	end)
end)

teardown(function()
	os.exit()
end)
