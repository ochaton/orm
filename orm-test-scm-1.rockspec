package = "orm-test"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/ochaton/orm.git",
}
description = {
   summary = "Handy library to hang own methods to tuples",
   homepage = "https://gitlab.com/ochaton/orm",
   license = "BSD 2"
}
dependencies = {
   "lua ~> 5.1",
   "luacov ~> 0.15",
   "luacov-console ~> 1.1",
   "luacov-multiple ~> 0.6",
   "argparse ~> 0.7",
   "luatest scm-1",
}
build = {
   type = "builtin",
}
