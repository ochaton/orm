.PHONY := all test


install-deps:
	tarantoolctl rocks install --only-deps orm-scm-1.rockspec

install-test-deps:
	tarantoolctl rocks install --only-deps orm-test-scm-1.rockspec

test: install-deps install-test-deps tmp
	.rocks/bin/luatest -c -v --coverage

tmp:
	mkdir $@
