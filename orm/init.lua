local Space = require 'orm.Space'
local log = require 'log'
local fun = require 'fun'
local class = require 'metaclass' --[[@as metaclass]]

--- Base class to create orm for Tarantool Spaces
---@class orm:metaclass.Class
---@field automigrate boolean developer flag which enables automigration (requires spacer)
---@field hint boolean should spaces and indexes use hints
---@field subscribe_to_ddl boolean
---@field spacer table link to spacer
---@field create_if_not_exists boolean
---@field ddl table object of tarantool/ddl
---@field space table<string,orm.Space> kv map of orm spaces
---@field Tuple orm.TupleClass? base class can be overwritten for each instance of orm
---@operator call(orm):orm
local orm = class "orm"

---Contructor of orm
---@param self orm
function orm:constructor()
	self.space = setmetatable({}, {
		__index = function(space, space_name)
			rawset(space, space_name, Space:new())
			return space[space_name]
		end,
	})

	self._space_watchers = {}
	self._index_watchers = {}

	if self.subscribe_to_ddl then
		self:register_space_trigger(nil, function(space)
			box.on_commit(function()
				if space.name == nil then
					log.warn("name of the space not defined")
					return
				end
				if not self.space[space.name] then
					self.space[space.name] = Space:new()
				end
				self.space[space.name]:build(space.name, { format = space.format }, self)
			end)
		end)
	end
end

local space_replace_trigger = '__orm_space_on_replace_trigger__'
local index_replace_trigger = '__orm_index_on_replace_trigger__'

---Sets on_replace on `box.space._space` for specific space
---@param space_name string?
---@param func fun(space: {id:integer,name:string,format:boxSpaceFormat})
function orm:register_space_trigger(space_name, func)
	if space_name then
		self._space_watchers[space_name] = func
	else
		self._any_space_watcher = func
	end
	rawset(_G, space_replace_trigger, box.space._space:on_replace(function(_, space, _, _)
		if not space then return end

		---@cast space +{name: string}
		local callback = self._space_watchers[space.name] or self._any_space_watcher

		if not callback then return end

		local ok, err = pcall(callback, space)
		if not ok then
			log.error("orm.on_replace_space trigger for %s failed: %s", space.name, err)
		end
	end, rawget(_G, space_replace_trigger)))
end

---Registers on_replace trigger for `box.space._index` space
---@param space_name string
---@param func fun()
function orm:register_index_trigger(space_name, func)
	self._index_watchers[space_name] = func

	rawset(_G, index_replace_trigger, box.space._index:on_replace(function(_, index, _, _)
        if not index then return end
		---@cast index +{id: integer, name:string}
		---@diagnostic disable-next-line: undefined-field
		local name = box.space._space:get(index.id).name
		local callback = self._index_watchers[name]

		if not callback then return end

		local ok, err = pcall(callback)
		if not ok then
			log.error("orm.on_replace_index trigger for %s/%s failed: %s",
				name, index.name or index.id, err)
		end
	end, rawget(_G, index_replace_trigger)))
end

---Entrypoint to configure orm object
---@param ddl {[string]: orm.SpaceDefinition }
function orm:configure(ddl)
	setmetatable(self.space, nil)

	if self.ddl then
		self.space._ddl_sharding_key = Space:new()
		self.space._ddl_sharding_key:build('_ddl_sharding_key', {
			format = {
				{ name = 'space_name',   type = 'string' },
				{ name = 'sharding_key', type = 'array'  },
			},
			primary = {'space_name'},
		}, self)
		self.space._ddl_sharding_func = Space:new()
		self.space._ddl_sharding_func:build('_ddl_sharding_func', {
			format = {
				{ name = 'space_name',         type = 'string' },
				{ name = 'sharding_func_name', type = 'string', is_nullable = true },
				{ name = 'sharding_func_body', type = 'string', is_nullable = true },
			},
			primary = {'space_name'},
		}, self)
	end

	local function kids(space)
		return fun.iter(ddl[space].format)
			:map(function(f) return f.ref end)
			:grep(fun.op.truth)
			:totable()
	end

	---runs dfs
	---@param root string
	---@param path {nodes: table<string,any>, inode: table<number, any>}?
	local function dfs(root, path)
		path = path or { inode = {}, nodes = {}, depth = 0 }
		local pref = ("DFS[%s]: %s"):format(path.depth, ("\t"):rep(path.depth))

		log.verbose(pref .. "> %s", root)

		-- If node is already gray -- we found circular dependency
		if path.nodes[root] and not ddl[root].circular then
			-- log.error("Circular references discovered: %s")
			local p = { root }
			for d = path.depth, 1, -1 do
				table.insert(p, path.inode[d])
				if path.inode[d] == root then break end
			end
			log.error("Circular path discovered: %s", table.concat(p, ' <- '))
			error(("Circular path: %s"):format(table.concat(p, ' <- ')))
		end

		-- Mark gray
		path.depth = path.depth + 1
		path.nodes[root] = path.depth
		path.inode[path.depth] = root

		for _, kid in ipairs(kids(root)) do
			if not path.nodes[kid] then
				dfs(kid, path)
			end
		end

		-- Remove from gray
		assert(path.inode[path.nodes[root]] == root, "Algorithm fucked up")
		path.depth = path.depth - 1
		path.nodes[root] = nil

		log.verbose(pref.."< %s", root)
	end

	for root in pairs(ddl) do
		dfs(root)
	end

	for space_name in pairs(ddl) do
		if not self.space[space_name] then
			self.space[space_name] = Space:new()
		end
	end
	for space_name, space_desc in pairs(ddl) do
		self.space[space_name]:build(space_name, space_desc, self)
	end
end

---Creates orm.Space from given box.space
---@param name_or_space string|boxSpaceObject
---@param casters {string:string}? field casters
---@param pretty {string:string}? field pretty printers
---@return orm.Space
function orm:from_source(name_or_space, casters, pretty)
	casters = casters or {}
	pretty = pretty or {}

	local box_space
	if type(name_or_space) == 'string' then
		box_space = box.space[name_or_space]
	elseif type(name_or_space) == 'table' then
		box_space = name_or_space
	end

	if box_space == nil then
		error("Space does not exists")
	end

	local fmt = assert(box_space:format())
	for _, f in ipairs(fmt) do
		if casters[f.name] then
			f.cast = casters[f.name]
		end
		if pretty[f.name] then
			f.pretty = pretty[f.name]
		end
	end

	local space = Space:new()
	space:build(box_space.name, {format = fmt}, self)
	return space
end

function orm:__serialize()
	local r = {}
	for k, v in pairs(self) do
		if k:sub(1, 1) ~= '_' then
			r[k] = v
		end
	end
	return r
end

---@generic T
---@param value T
---@return fun(): T
function orm.default(value)
	return function() return value end
end

return orm