---
---@class orm.Space.FieldFormat
---@field name string name of the tuple field
---@field type string type of the tuple field
---@field auto? string|function default builder of value of the field

local log = require 'log'
local class = require 'metaclass'

---@class orm.Field
---@field name string name of the field
---@field type string type of the field
---@field is_nullable boolean? flag whether field can be nullable
---@field auto? (fun(orm.Space, orm.Tuple): tuple_type)|"uuid"|"time64"|"time"
---@field ref? string string name of the space for reference
---@field pretty? string pretty cast for field

---@class orm.Space:metaclass.Class
---@field tomap fun(box.tuple): table<string,tuple_type>
---@field name string name of the space
---@field id integer ID of the space
---@field fields orm.Field[] list of names of fields
---@field space boxSpaceObject var of the box.space
---@field refs table<string,orm.Space> map of references for each field to orm.Space
---@field fmap table<string,orm.Space.FieldFormat> info about each field in space
---@field f2id table<string,number> mapping from field name to field id
---@field index table<string|number,orm.Index> list of indexes
---@field casts table<string,fun(value: tuple_type, type: string):boolean, any>
---@field new fun(orm.Space?):orm.Space
---@field circular boolean is space allows circular references to itself
local Space = class 'orm.Space'

local Index = require 'orm.Index'
local Tuple = require 'orm.Tuple'
local fun   = require 'fun'
local ffi   = require 'ffi'
local uuid  = require 'uuid'
local clock = require 'clock'
local fiber = require 'fiber'
local json = require 'json'
local human_size = require 'orm.util'.human_size

local isa = class.Object.methods.isa

local function tomap(t)
	assert(type(t) == 'table', 'tomap requires tables')
	local mt = getmetatable(t)
	if not mt then
		return setmetatable(t, {__serialize = 'map'})
	end
	mt.__serialize = 'map'
	return t
end

local function toarray(t)
	assert(type(t) == 'table', 'tomap requires tables')
	local mt = getmetatable(t)
	if not mt then
		return setmetatable(t, {__serialize = 'array'})
	end
	mt.__serialize = 'array'
	return t
end

local function cast(value, need_type)
	if type(value) == need_type then return true end
	if need_type == '*' then return true end

	if need_type == 'uuid' then
		if type(value) == 'cdata' then return true end
		if type(value) == 'string' then
			local casted = uuid.fromstr(value) or uuid.frombin(value)
			return not (casted == nil), casted
		end
	elseif need_type == 'decimal' then
		return type(value) == 'cdata'
	elseif need_type == 'integer' or need_type == 'unsigned' then
		return type(value) == 'number' or
			ffi.istype('uint64_t', value) or ffi.istype('int64_t', value)
	elseif need_type == 'map' then
		if type(value) == 'table' then return true, tomap(value) end
		return false
	elseif need_type == 'array' then
		if type(value) == 'table' then return true, toarray(value) end
		return false
	elseif need_type == 'any' or need_type == '*' then
		return true
	end
	return false
end

local function cast_hex(value, what)
	if type(value) ~= 'string' then
		return value
	end
	if what == 'pack' then
		return value:fromhex()
	elseif what == 'unpack' then
		return value:hex():upper()
	end
end

local function pretty_datetime(value)
	return os.date("%FT%T", tonumber(value) or 0)
end

local function pretty_bytes(bytes)
	return human_size(bytes)
end

local function only_format(list)
	local fmt = {}
	for _, line in ipairs(list) do
		table.insert(fmt, {
			name = line.name,
			type = line.type,
			is_nullable = line.is_nullable,
		})
	end
	return fmt
end

local function autogen(generator, orm_space, orm_tuple)
	if generator == 'uuid' then
		return uuid()
	elseif generator == 'time64' then
		return clock.time64()
	elseif generator == 'time' then
		return math.floor(fiber.time())
	elseif type(generator) == 'function' then
		return generator(orm_space, orm_tuple)
	else
		error(("Unknown auto generator: %s"):format(generator))
	end
end

function Space:pp(fname, value)
	local pp = self.pretty[fname]
	if not pp then
		return value
	end
	return pp(value)
end

function Space:__serialize()
	if not self.space then
		return "Space<"..self.name..":not-created"..">"
	end
	local total_size = self.space:bsize()
	local indsize = 0
	for i = 0, #self.space.index do
		indsize = indsize + self.space.index[i]:bsize()
	end
	total_size = total_size + indsize
	return ("Space<%s:%s:%s/%s#%s(%.2f%%)=%s+%s>"):format(
		self.name, self.space.id, self.space.engine, self:len(),
		human_size(tonumber(total_size)),
		tonumber(100*total_size / box.slab.info().arena_used),
		human_size(tonumber(self.space:bsize())),
		human_size(tonumber(indsize))
	)
end

function Space:build_indexes()
	self.index = {} --[[@as table<string|number,orm.Index> ]]
	for no = 0, #self.space.index do
		local ind = self.space.index[no]
		if ind then
			local index = Index:new(self.space.index[no], self)
			self.index[no], self.index[index.name] = index, index
		else
			log.warn("index %d not exists for %s", no, self.name)
		end
	end
	if self.sharding_enabled then
		self:build_sharding(self.sharding_func, self.sharding_key)
		log.info("ddl: setting schema for %s", self.name)
		box.space._ddl_sharding_key:replace({self.name, self.sharding_key})
		box.space._ddl_sharding_func:replace({self.name, self.sharding_func, box.NULL})
	end
end

local vshard do
	local ok
	ok, vshard = pcall(require, 'vshard')
	if not ok then
		vshard = nil
	end
end

rawset(_G, '__orm_default_sharding_func__', function(key)
	assert(vshard, 'vshard is not installed')
	return vshard.router.bucket_id_mpcrc32(key)
end)

function Space:build_sharding(func, key)
	self.sharding_func = func or '__orm_default_sharding_func__'
	self.sharding_key = key or self.index[0].parts
	assert(type(self.sharding_func) == 'string', ("Space[%q]: sharding_func must be a string"):format(self.name))
	assert(type(self.sharding_key) == 'table', ("Space[%q]: sharding_key must be list of fields"):format(self.name))
	assert(#self.sharding_key > 0, ("Space[%q]: sharding_key must be a not empty list of fields"):format(self.name))
	local fbody = {}
	for _, fname in ipairs(self.sharding_key) do
		assert(self.f2id[fname], ("Space[%q]: does not contain field %q"):format(self.name, fname))
		table.insert(fbody, 't["'..fname..'"]')
	end
	log.info("Building sharding %s(%s) for space %s", self.sharding_func, json.encode(self.sharding_key), self.name)
	self.sharding = dostring(
		"return function(t) return "..table.concat(fbody, ",").." end"
	)
	self.bucket_id = dostring(
		"return function(t) return "..self.sharding_func.."{"..table.concat(fbody, ",").."} end"
	)
end

function Space:_build_class()
	self.casts = fun.iter(self.fields)
		:grep(function(f) return f.cast end)
		:map(function(f)
			if type(f.cast) == 'function' then
				return f.name, f.cast
			elseif f.cast == 'hex' then
				return f.name, cast_hex
			else
				error("Unknown cast function "..tostring(f.cast))
			end
		end)
		:tomap()

	self.pretty = fun.iter(self.fields)
		:grep(function(f) return f.pretty end)
		:map(function(f)
			if type(f.pretty) == 'function' then
				return f.name, f.pretty
			elseif f.pretty == 'datetime' then
				return f.name, pretty_datetime
			elseif f.pretty == 'bytes' then
				return f.name, pretty_bytes
			else
				error("Unknown pretty function "..tostring(f.pretty))
			end
		end)
		:tomap()

	self.fmap = fun.iter(self.fields)
		:map(function(f) return f.name, f end)
		:tomap()

	self.f2id = fun.iter(self.fields)
		:enumerate()
		:map(function(no, f) return f.name, no end)
		:tomap()

	local fbody = {}
	for i, f in ipairs(self.fields) do
		table.insert(fbody, ("[%q] = defined(t[%d])"):format(f.name, i))
	end
	self.tomap = dostring(
		"local function defined(v) if v == nil then return end return v end "..
		"return function (t) return {"..table.concat(fbody, ",").."} end"
	)
	if self.class and not self.class.methods._space and self.class.methods.withSpace then
		self.tuple = function(box_tuple)
			if not box_tuple then return end
			return self.class(self.tomap(box_tuple)):withSpace(self)
		end
	else
		self.tuple = function(box_tuple)
			if not box_tuple then return end
			return self.class(self.tomap(box_tuple))
		end
	end
end

function Space:_rebuild_fields_from_format()
	self.fields = setmetatable(self.space:format() or {}, {__serialize='sequence'})
	self:_build_class()
end

--luacheck:ignore
---@alias ormReplaceTrigger
---| fun(old: orm.Tuple|nil, new: orm.Tuple|nil, space: orm.Space, operation: "UPDATE"|"DELETE"|"REPLACE"|"INSERT"|"UPSERT"): orm.Tuple?

---Builds trigger func
---@param func ormReplaceTrigger
---@return replaceTrigger
function Space:build_trigger_func(func)
	return function(old_tuple, new_tuple, _, operation)
		local old = self.tuple(old_tuple)
		local new = self.tuple(new_tuple)

		local res = func(old, new, self, operation)
		if res == nil then
			return
		end
		if res == old then return old_tuple end
		if res == new then return new_tuple end

		return box.tuple.new(self:totable(res))
	end
end


---@class orm.SpaceDefinition
---@field format orm.Field[] format of the fields
---@field primary? string[]|{hint: boolean} list of fields to be used for primary index
---@field secondaries? {name: string, parts: string[], hint: boolean? }[] list of secondary indexes (they all be unique)
---@field temporary? boolean is space should be temporary
---@field is_local? boolean is space should be local
---@field is_sync? boolean is space should use synchronous transactions
---@field hint? boolean should indexes have hint or not
---@field create_if_not_exists? boolean if `true` create space only when it is not exist
---@field sharding_func? string name of the sharding_func (must be created in box.space._func)
---@field sharding_key? string[] list of fields in tuple used for sharding
---@field sharding_enabled? boolean flag which enables or disables sharding for given space
---@field circular? boolean allows circular dependencies for space
---@field class? orm.TupleClass class of the tuples to be casted

---Builds space from definition
---@param name string space name
---@param opts orm.SpaceDefinition
---@param orm orm
function Space:build(name, opts, orm)
	log.info("Building space %s", name)
	for k, v in pairs(opts) do
		self[k] = v
	end

	self.name = name
	self.refs = {}

	self.triggers = {
		before = {},
		on = {},
	}

	local BaseTuple = orm.Tuple or Tuple

	if opts.format then
		for _, field in ipairs(opts.format) do
			if field.ref then
				self.refs[field.name] = orm.space[field.ref]
			end
		end
	end

	if not opts.class then
		local class_name = BaseTuple:name().."<"..self.name..">"
		self.class = class[class_name] or (class(class_name) : inherits(BaseTuple) : prototype {
			_space = self
		})
	else
		assert(opts.class.methods._space, "_space must be defined")
		self.class = opts.class
	end

	if orm.spacer then
		assert(opts.primary, "primary index is required for space: "..self.name)
		self.fields = assert(opts.format, "format is required for space: "..self.name)

		local indexes = fun.iter(opts.secondaries or {})
			:map(function(desc) return { name = desc.name, parts = desc.parts } end)
			:totable()

		table.insert(indexes, 1, { name = 'primary', parts = opts.primary })

		orm.spacer:space {
			name = name,
			format = only_format(self.fields),
			indexes = indexes,
			opts = { temporary = opts.temporary, is_local = opts.is_local },
		}

		if orm.automigrate then
			orm.spacer:automigrate()
			self.space = box.space[self.name]
		end
	elseif opts.create_if_not_exists or orm.create_if_not_exists then
		assert(opts.primary, "primary index is required for space: "..self.name)
		self.fields = assert(opts.format, "format is required for space: "..self.name)

		self.space = box.schema.space.create(name, {
			if_not_exists = true,
			temporary = opts.temporary,
			is_local = opts.is_local,
			is_sync = opts.is_sync,
		})
		self.space:format(only_format(self.fields))
		if opts.primary then
			local hint
			if opts.primary.hint ~= nil then
				hint = opts.primary.hint
			elseif orm.hint ~= nil then
				hint = orm.hint
			end

			self.space:create_index('primary', {
				if_not_exists = true,
				parts = fun.totable(opts.primary),
				hint = hint,
			})
		end
		for _, desc in ipairs(opts.secondaries or {}) do
			local hint
			if desc.hint ~= nil then
				hint = desc.hint
			elseif opts.hint ~= nil then
				hint = opts.hint
			elseif orm.hint ~= nil then
				hint = orm.hint
			end
			self.space:create_index(desc.name, {
				if_not_exists = true,
				parts = desc.parts,
				hint = hint,
			})
		end
	end

	if box.space[name] then
		self.space = box.space[name]
		self.fields = assert(opts.format, "format is required for space: "..self.name)
	end

	if self.fields then
		setmetatable(self.fields, {__serialize='sequence'})
		for _, f in ipairs(self.fields) do
			setmetatable(f, {__serialize='map'})
		end
	end

	if opts.sharding_func or opts.sharding_key then
		self.sharding_enabled = true
		self.sharding_func = opts.sharding_func
		self.sharding_key = opts.sharding_key
	end

	if opts.sharding_enabled ~= nil then
		self.sharding_enabled = opts.sharding_enabled
	end

	if self.space then
		self:_build_class()
		self:build_indexes()
	else
		orm:register_space_trigger(name, function(box_space)
			assert(box_space.name == name, name..": not my space")
			box.on_commit(function()
				self.space = assert(box.space[box_space.name], "no space")
				fiber.create(self._rebuild_fields_from_format, self)
			end)
		end)
	end
	if not self.space or #self.space.index == 0 then
		orm:register_index_trigger(name, function()
			box.on_commit(function()
				fiber.create(self.build_indexes, self)
			end)
		end)
	end
end

---Extracts key from tuple
---@param tuple orm.Tuple
---@return unknown
function Space:fold(tuple)
	if isa(tuple, Tuple) then
		return self.index[0]:key(tuple)[1]
	else
		return tuple
	end
end

function Space:pairs(...)
	return self.index[0]:pairs(...)
end

function Space:get(...)
	return self.index[0]:get(...)
end

function Space:select(...)
	return self.index[0]:select(...)
end

function Space:update(key, kv)
	local toupdate = {}
	for fname, nv in pairs(kv) do
		if self.fmap[fname] then
			if isa(nv, Tuple) then
				table.insert(toupdate, { '=', self.f2id[fname], nv:getkey() })
			else
				table.insert(toupdate, { '=', self.f2id[fname], nv })
			end
		elseif fname:find('.') then
			local path = fname:split('.')
			local def = self.fmap[path[1]]
			if def and (def.type == 'map' or def.type == 'array' or def.type == '*' or def.type == 'any') then
				if isa(nv, Tuple) then
					nv = nv:getkey()
				end
				local t = key
				for i = 1, #path-1 do
					if t[path[i]] == nil then
						t[path[i]] = {}
					end
					t = t[path[i]]
				end
				t[path[#path]] = nv

				table.insert(toupdate, {'=', self.f2id[path[1]], key[path[1]]})
			end
		end
	end

	return self.index[0]:update(key, toupdate)
end

function Space:delete(...)
	return self.index[0]:delete(...)
end

function Space:len()
	if ({service=true,sysview=true})[self.space.engine] and self.space.id < 512 then
		return self.space:count()
	end
	return self.space:len()
end

function Space:totable(record)
	local list = {}
	for i, f in ipairs(self.fields) do
		if type(record[f.name]) ~= 'nil' then
			local ok, casted = cast(record[f.name], f.type)
			if not ok then
				if self.refs[f.name] then
					list[i] = self.refs[f.name]:fold(record[f.name])
				else
					error("space."..self.name..": field '"..f.name.."' has type '"
						..type(record[f.name]).."' required '"..f.type.."'", 4)
				end
			else
				list[i] = casted or record[f.name]
			end
		else -- record[f.name] is nil
			if f.auto then
				list[i] = autogen(f.auto, self, record)
			elseif not f.is_nullable then
				error("space."..self.name..": field '"..f.name.."' is required but not given")
			end
		end
	end
	return list
end

---@return orm.Tuple
function Space:insert(record)
	return assert(self.tuple(self.space:insert(self:totable(record))))
end

---comment
---@param record orm.Tuple
---@return orm.Tuple
function Space:replace(record)
	return assert(self.tuple(self.space:replace(self:totable(record))))
end

function Space:max(...)
	return self.index[0]:max(...)
end

function Space:min(...)
	return self.index[0]:min(...)
end

---Flushes tuple into space
---@param record orm.Tuple
---@return orm.Tuple
function Space:flush(record)
	if not box.is_in_txn() then
		log.warn("Space<%s> It is strongly desirable to call Space:flush inside transaction",
			self.name)
	end
	for fname, ref in pairs(self.refs) do
		if record[fname] ~= nil then
			record[fname] = ref:fold(ref:flush(record[fname]))
		end
	end
	return self:replace(record)
end

---Extracts primary key from given tuple
---@param tuple any
---@return string[]
function Space:getpk(tuple)
	return self.index.primary:extract(tuple)
end

---Sets on_replace trigger
---@param name string
---@param func ormReplaceTrigger
---@return ormReplaceTrigger # new trigger
function Space:on_replace(name, func)
	self.triggers.on[name] = self.space:on_replace(self:build_trigger_func(func), self.triggers.on[name])
	return self.triggers.on[name]
end

---Sets before_replace trigger
---@param name string name of the trigger
---@param func ormReplaceTrigger the trigger
---@return ormReplaceTrigger # new trigger
function Space:before_replace(name, func)
	self.triggers.before[name] = self.space:before_replace(self:build_trigger_func(func), self.triggers.before[name])
	return self.triggers.before[name]
end

function Space:clear_triggers()
	for name, func in pairs(self.triggers.on) do
		self.triggers.before[name] = nil
		self.space:on_replace(nil, func)
	end
	for name, func in pairs(self.triggers.before) do
		self.triggers.before[name] = nil
		self.space:before_replace(nil, func)
	end
end

return Space