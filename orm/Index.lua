local class = require 'metaclass'
local Tuple = require 'orm.Tuple'
local Plan  = require 'orm.Plan'

---@class orm.Index
---@field new fun(orm.Index, boxIndex, orm.Space): orm.Index
---@field box boxIndex link to box.index
---@field id number index id
---@field name string index name
---@field space orm.Space link to space
---@field type string index type
---@field parts string[] list of field names used for index
---@operator call(): orm.Index
local Index = class 'orm.Index'

local fun   = require 'fun'
local log   = require 'log'
local uuid  = require 'uuid'
local decimal do
	local ok
	ok, decimal = pcall(require, 'decimal')
	if not ok then
		decimal = nil
	end
end
local fiber = require 'fiber'
local isa = Index.methods.isa
local human_size = require 'orm.util'.human_size

---Creates new index
---@param box_index boxIndex
---@param space orm.Space
function Index:constructor(box_index, space)
	self.box   = box_index
	self.id    = box_index.id
	self.name  = box_index.name
	self.space = space
	self.unique = box_index.unique
	self.type   = box_index.type

	self.parts = fun.iter(self.box.parts)
		:map(function(part) return space.fields[part.fieldno].name end)
		:totable()
	log.info("Index %s/%d created for space %s", self.name, self.id, self.space.name)
end

local function cast(itype, value)
	if itype == 'uuid' then
		if type(value) == 'string' then
			return uuid.fromstr(value) or uuid.frombin(value)
		end
	elseif itype == 'decimal' then
		if decimal == nil then
			error("Decimals are not supported in this version of Tarantool")
		end
		return decimal.new(value)
	end
	return value
end

---Extract key from given argument
---@param arg orm.Tuple|{[string]:any}
---@return any[]
function Index:key(arg)
	if arg == nil then return {} end

	if type(arg) ~= 'table' or isa(arg, Tuple) then
		arg = { arg }
	end

	if #arg == 0 and next(arg) then -- k/v
		local fields = {}
		for i, fname in ipairs(self.parts) do -- iterate through parts of index
			fields[i] = arg[fname]
		end

		arg = fields
	end

	local res = {}
	for i, field in ipairs(arg) do
		local fname = self.parts[i]

		if isa(field, Tuple) then
			-- orm.Tuple given:
			if self.space.refs[fname] then
				field = self.space.refs[fname]:fold(field)
			else
				field = field[fname]
			end
		end

		local packer = self.space.casts[fname]
		if packer then
			res[i] = packer(field, 'pack')
		else
			local itype = self.box.parts[i].type
			if itype ~= type(field) then
				res[i] = cast(itype, field) or field
			else
				res[i] = field
			end
		end

	end
	return res
end

---Extracts index keys from given argument
---@param arg table
---@return string[] index_values
function Index:extract(arg)
	local fields = {}
	if type(arg) ~= 'table' then
		arg = {arg}
	end
	if #arg > 0 then
		for _, p in ipairs(self.box.parts) do
			table.insert(fields, arg[p.fieldno])
		end
	else
		for _, f in ipairs(self.parts) do
			table.insert(fields, arg[f])
		end
	end

	local r = {}
	for i, f in ipairs(fields) do
		if isa(f, Tuple) then
			-- fold?
			f = self.space.refs[self.parts[i]]:fold(f)
		end
		table.insert(r, cast(self.box.parts[i].type, f))
	end

	return r
end

local function yield_gen_x(i, state_x, ...)
	if state_x == nil then
		return nil
	end
	return {i, state_x}, ...
end

local function yield_gen(param, state)
	local n, no, gen_x, param_x = param[1], param[2], param[3], param[4]
	local i, state_x = state[1], state[2]
	if i % n == 0 then
		fiber.sleep(no)
	end
	return yield_gen_x(i+1, gen_x(param_x, state_x))
end

local function yield1(n, no, gen, param, state)
	assert(n >= 0, "invalid first argument to yield")
	assert(no >= 0, "invalid second argument to yield")
	return fun.wrap(yield_gen, { n, no, gen, param }, { 0, state })
end

local function method_yield(self, n, no)
	return yield1(n, no or 0, self.gen, self.param, self.state)
end


---@class IndexIteratorOptions: boxTableIterator
---@field yield number (default: 100) number of iterations to perform without yield
---@field yield_sleep number (default: 0) number of seconds to sleep during yield
---@field offset number iterator offset
---@field limit number iterator limit
---@field raw boolean (default: false) returns raw iterator instead tuple iterator

---Index iterator
---@param arg any
---@param opts IndexIteratorOptions?
---@return fun.iterator
function Index:pairs(arg, opts)
	local iterator = self.box:pairs(self:key(arg), opts)
	if opts then
		if opts.yield or fiber.self().storage.console and not box.is_in_txn() then
			iterator = method_yield(iterator, opts.yield or 100, opts.yield_sleep)
		end
		if opts.offset then
			iterator = iterator:drop_n(opts.offset)
		end
		if opts.limit then
			iterator = iterator:take_n(opts.limit)
		end
		if opts.raw then
			return iterator
		end
	end

	return iterator:map(self.space.tuple)
end

---Creates complex execution plan
---@param key any
---@param opts IndexIteratorOptions
---@return orm.Plan
function Index:q(key, opts)
	return Plan{ index = self, key = key, opts = opts, tomap = self.space.tuple }
end

---comment
---@param arg any
---@return orm.Tuple?
function Index:get(arg)
	return self.space.tuple(self.box:get(self:key(arg)))
end

---Selects tuples from index by given key
---@param arg any
---@param opts IndexIteratorOptions
---@return (box.tuple|orm.Tuple)[]
function Index:select(arg, opts)
	return self:pairs(arg, opts):totable()
end

function Index:update(key, ups)
	return self.space.tuple(self.box:update(self:extract(key), ups))
end

---Deletes tuple from index by given key
---@param key any
---@return orm.Tuple?
function Index:delete(key)
	return self.space.tuple(self.box:delete(self:extract(key)))
end

---Returns maximum tuple for given key
---@param key? any
---@return orm.Tuple?
function Index:max(key)
	return self.space.tuple(self.box:max(self:key(key)))
end

---Returns minimum tuple for given key
---@param key? any
---@return orm.Tuple?
function Index:min(key)
	return self.space.tuple(self.box:min(self:key(key)))
end

---Alters index
---@param opts boxIndexOptions
---@return nil
function Index:alter(opts)
	return self.box:alter(opts)
end

function Index:__serialize()
	return ("Index<%s/%s:%s:%s#%s>{%s}"):format(
		self.space.name, self.name,
		self.box.hint and "hints" or "no-hints",
		self.box.unique and "unique" or "non-unique",
		human_size(tonumber(self.box:bsize())),
		table.concat(self.parts, ",")
	)
end

return Index
