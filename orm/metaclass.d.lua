---@meta

---@class metaclass
---@operator call(string): metaclass.Class
local ClassBuilder = {}

---@class metaclass.Class
local Class = {}

---Instantiates new object
---@generic T:metaclass.Object
---@param obj? table|T constructor arguments
---@param ... any?
---@return T
function Class:new(obj, ...) end

Class.instantiate_object = Class.new

---@param parentClass string|metaclass.Class
---@return metaclass.Class
function Class:inherits(parentClass) end

---@return string
function Class:name() end

---Disables autocast of object
---@return metaclass.Class
function Class:noautocast() end

---Sets prototype fields and methods into the class
---@param proto table
---@return metaclass.Class
function Class:prototype(proto) end

---@class metaclass.Object
local Object = {}

---@param class string|metaclass.Class
---@return boolean
function Object:isa(class) end

---Returns parent class of this object
---@return metaclass.Class
function Object:super() end

---Returns name of the class of the instantiated object
---@return string
function Object:who() end

---@return metaclass.Object
function Object:copy() end

return ClassBuilder
