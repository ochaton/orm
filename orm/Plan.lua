local class = require 'metaclass'
local fun = require 'fun'

---@class orm.Plan
---@field private index orm.Index
---@field private space orm.Space
---@field private opts { raw: boolean, use_tomap: boolean }
---@field private key any
---@field private tomap fun(box.tuple): orm.Tuple
local Plan = class "orm.Plan"


function Plan:constructor()
	assert(self.index, "Plan: index is required")
	self.space = self.index.space

	self.opts = self.opts or {}
	if self.opts.raw == nil then
		self.opts.raw = true
		self.use_tomap = true
	end

	self.orders = {}

	return self
end

---@enum orm.Plan.operations
local operations = {
	['='] = '==',
	['=='] = '==',
	['>='] = '>=',
	['<='] = '<=',
	['<'] = '<',
	['>'] = '>',
	['~='] = '~=',
	['!='] = '~=',
	["in"] = 'has',
}

local orders = {
	ASC  = function(key, t1, t2) return t1[key] < t2[key] end,
	DESC = function(key, t1, t2) return t2[key] < t1[key] end,
}

---Sets where filter for index
---@param conds { [1]: orm.Plan.operations, [2]: string, [3]: any  }[]
---@return orm.Plan
function Plan:where(conds)
	local fbody = {}
	local prolog = ''
	for i, cond in ipairs(conds) do
		local op, key, value = cond[1], cond[2], cond[3]

		local filter = operations[op]
		assert(filter, "operation: "..tostring(op).." is not supported")

		local key_no = self.space.f2id[key]
		assert(key_no, "key "..tostring(key).." is not defined in space "..self.space.name)

		if op == 'in' then
			local tabname = "__"..key_no.."_"..i
			prolog = prolog .. " local "..(tabname.." = {%s}"):format(
				table.concat(
					fun.iter(value)
						:map(function(t)
							if type(t) == 'string' then
								return ("[%q] = true"):format(t)
							else
								return ("[%s] = true"):format(t)
							end
						end)
						:totable(),
					","
				)
			)
			table.insert(fbody, tabname..("[t[%s]]"):format(key_no))
		else
			if self.space.fields[key_no].type == 'string' then
				table.insert(fbody, ("t[%s] %s %q"):format(key_no, filter, value))
			else
				table.insert(fbody, ("t[%s] %s %s"):format(key_no, filter, value))
			end
		end
	end

	fbody = prolog.." return function(t) return "..table.concat(fbody, " and ").." end"
	self.cond = dostring(fbody)

	return self
end

---Sets sort order for query
---@param conds { [1]: string, [2]: "ASC"|"DESC" }[]
---@return orm.Plan
function Plan:order(conds)
	for i = #conds, 1, -1 do
		local cond = conds[i]
		local key = cond[1]
		local order = (cond[2] or "ASC"):upper()

		local cmp = orders[order]
		assert(cmp, "Order type "..tostring(order).." is not supported")

		table.insert(self.orders, function(t1, t2) return cmp(key, t1, t2) end)
	end
	return self
end

---Executes request, and returns first `n` records
---@param n? integer
---@return orm.Tuple[]
function Plan:first(n)
	n = n or 1
	local iterator = self.index:pairs(self.key, self.opts)

	if self.cond then
		iterator = iterator:filter(self.cond)
	end

	local rows
	if #self.orders > 0 then
		rows = iterator:totable()
		for _, order in ipairs(self.orders) do
			table.sort(rows, order)
		end
	else
		iterator = iterator:take(n)
	end

	if self.use_tomap then
		if rows then
			iterator = fun.iter(rows)
		end
		iterator = iterator:map(self.tomap)
	end

	return iterator:totable()
end

function Plan:all()
	return self:first(math.huge)
end

return Plan