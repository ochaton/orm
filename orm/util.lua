local function human_size(bytes)
	if bytes <= 1024 then
		return bytes..'b'
	elseif bytes <= 1024*1024 then
		return ('%.2fKb'):format(bytes/1024)
	elseif bytes <= 1024*1024*1024 then
		return ('%.2fMb'):format(bytes/1024/1024)
	elseif bytes <= 1024*1024*1024*1024 then
		return ('%.2fGb'):format(bytes/1024/1024/1024)
	else
		return ('%.2fTb'):format(bytes/1024/1024/1024/1024)
	end
end

return {
	human_size = human_size,
}