local json = require 'json'
local class = require 'metaclass'

---@class orm.TupleClass
---@field new fun(orm.TupleClass?): orm.TupleClass
---@field name fun(orm.TupleClass): string
---@field methods table<string, fun(orm.TupleClass, ...:any): ...:any> map of class methods
---@operator call(any): orm.Tuple

---@class orm.Tuple:metaclass.Class
---@field withSpace fun(orm.Tuple, orm.Space): orm.Tuple
---@field _space orm.Space? link to the space
local Tuple = class "Tuple"


function Tuple:constructor()
	if self._space then self:unfold() end
end

---Follows n levels of references and returns merged object.
---
---Changes given object itself
---@param n? number
---@return orm.Tuple
function Tuple:unfold(n)
	assert(self._space, "space is required")
	if n == 0 then return self end
	for fname, cast in pairs(self._space.casts) do
		self[fname] = cast(self[fname], 'unpack')
	end
	for fname, ref_space in pairs(self._space.refs) do
		if self[fname] ~= nil then
			if ref_space == self._space and self._space.circular and self[fname] == self:getpk()[1] then
				self[fname] = self
			else
				self[fname] = ref_space:get(self[fname]) or self[fname]
			end
		end
	end
	return self
end

---Folds all references to flat structure
---
---Changes given object itself
---@return orm.Tuple
function Tuple:fold()
	assert(self._space, "space is required")
	for fname, cast in pairs(self._space.casts) do
		self[fname] = cast(self[fname], 'pack')
	end
	for ref in pairs(self._space.refs) do
		if self[ref] ~= nil then
			self[ref] = self._space.refs[ref]:fold(self[ref])
		end
	end
	return self
end

---Returns new table with pretty printed fields
---Requires casts
---@return table<string,any>
function Tuple:pretty()
	local r = {}
	for _, field in ipairs(self._space.fields) do
		local fname = field.name
		r[fname] = self._space:pp(fname, self[fname])
	end

	return r
end

---Extracts primary key of tuple
---@return unknown
function Tuple:getkey()
	return self._space:fold(self)
end

function Tuple:getpk()
	return self._space:getpk(self)
end

function Tuple:flush()
	return Tuple.copy(self, self._space:flush(self))
end

function Tuple:delete()
	return Tuple.copy(self, self._space:delete(self))
end

function Tuple:update(kv)
	return Tuple.copy(self, self._space:update(self, kv))
end

---@param from orm.Tuple
function Tuple:copy(from)
	assert(from:isa(Tuple))
	table.clear(self)

	for key, value in pairs(from) do
		self[key] = value
	end
	return self
end

function Tuple:__tostring()
	return json.encode(self)
end

return Tuple