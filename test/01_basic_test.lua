local t = require 'luatest'
local fio = require 'fio'
local fun = require 'fun'

local root = fio.dirname(fio.dirname(fio.abspath(debug.getinfo(1, 'S').source:sub(2))))

---@type orm
local orm = require 'orm'

local g = t.group('basic')

local function drop_user_spaces()
	for _, s in box.space._space:pairs({512},{iterator = 'GE'}) do
		box.space[s.id]:drop()
	end
end

g.before_all(function()
	box.cfg{checkpoint_count = 1}
	drop_user_spaces()
	box.snapshot()
end)

g.after_all(function() drop_user_spaces() box.snapshot() end)

---@param group luatest.group
---@diagnostic disable-next-line: inject-field
g.test_creation = function(group)
	local ddl = orm { create_if_not_exists = true }

	ddl:configure {
		users = {
			format = {
				{ name = 'id', type = 'unsigned' },
				{ name = 'email', type = 'string' },
			},
			primary = {'id'},
			secondaries = {
				{ name = 'email', parts = {'email'} }
			}
		}
	}

	t.assert_is(ddl.space.users.space, box.space.users, "space.users was created")
	t.assert_is(#ddl.space.users.index, #box.space.users.index, "spaces.users.index created")

	t.assert(box.space.users.index[0], "users.index[0] created")
	t.assert(box.space.users.index[1], "users.index[1] created")

	t.assert(ddl.space.users.index[0], "users.index[0] created")
	t.assert(ddl.space.users.index[1], "users.index[1] created")

	local u = ddl.space.users:insert{ id = 0, email = 'user@email' }
	t.assert(u, "tuple returned")
	t.assert_type(u, "table", "ddl:insert returns orm.Tuple")
	t.assert_is(u.id, 0, "id correct")
	t.assert_is(u.email, 'user@email', "email correct")

	t.assert_covers(u, box.space.users:get(0):tomap({names_only = true}), "tuple was inserted into space")

	u:update({ email = 'resu@email' })
	t.assert_is(u.email, 'resu@email', "email field was updated in place")
	t.assert_covers(u, box.space.users:get(0):tomap({names_only = true}), "tuple was updated in the space")

	-- delete tuple
	u:delete()
	t.assert(u, "tuple was deleted but stays visible")
	t.assert_not(box.space.users:get(0), "tuple was deleted from space")

	-- resurrect tuple
	u:flush()
	t.assert(u, "tuple was resurrected in the space")
	t.assert(box.space.users:get(0), "tuple was resurrected")

	t.assert_is(ddl.space.users:get({0}).id, 0, ":get{0}.id == 0")
	t.assert_is(ddl.space.users:get({0}).email, 'resu@email', ":get{0}.email == resu@email")

	t.assert_is(ddl.space.users:get({ u }).id, 0, ":get({ user })")
	t.assert_is(ddl.space.users:get(u).id, 0, ":get( user )")
	t.assert_is(ddl.space.users:get({ id = u }).id, 0, ":get({ id = user })")

	t.assert_is(ddl.space.users.index.email:get({ email = u }).id, 0, "index.email:get({ email = user })")
	t.assert_is(ddl.space.users.index.email:get({ u }).id, 0, "index.email:get({ user })")
	t.assert_is(ddl.space.users.index.email:get(u).id, 0, "index.email:get({ user })")

	u = nil
	box.space.users:truncate()
	t.assert_is(ddl.space.users:len(), 0, "space is empty")

	local users = {
		{ id = 0, email = 'cab@icsav.dk' },
		{ id = 2, email = 'tufefem@mum.pm' },
		{ id = 3, email = 'sokrin@zun.az' },
		{ id = 4, email = 'hufvueb@lashasjom.es' },
		{ id = 6, email = 'das@vazivrip.fr' },
	}

	local email2user = fun.map(function(user) return user.email, user end, users):tomap()

	for _, user in ipairs(users) do ddl.space.users:replace(user) end
	t.assert_is(ddl.space.users:len(), #users, "all users were inserted")

	local us = ddl.space.users:pairs():totable()
	t.assert_is(#us, #users, "all tuples were selected")
	t.assert_covers(us, users, "all seleted tuples were covered")
	t.assert_covers(ddl.space.users:select(), users, "all seleted tuples were covered")

	local index = assert(ddl.space.users.index.email, "email index")
	t.assert_type(users[1].email, 'string', 'check data')

	t.assert_items_equals(index:key(users[1].email), { users[1].email }, ":key(email) == {email}")
	t.assert_items_equals(index:key({ users[1].email }), { users[1].email }, ":key({ email }) == {email}")
	t.assert_items_equals(index:key({ email = users[1].email }), { users[1].email }, ":key({ email = email }) == {email}")
	t.assert_items_equals(index:key(users[1]), { users[1].email }, ":key({ email = email }) == {email}")

	local user = ddl.space.users.class(users[1])

	t.assert_items_equals(index:key(user.email), { users[1].email }, ":key(email) == {email}")
	t.assert_items_equals(index:key({ user.email }), { users[1].email }, ":key({ email }) == {email}")
	t.assert_items_equals(index:key({ email = user.email }), { users[1].email }, ":key({ email = email }) == {email}")

	-- t.assert_items_equals(index:extract({ user.email }), { users[1].email }, ":extract({ email }) == {email}")
	-- t.assert_items_equals(index:extract(user.email), { users[1].email }, ":extract(email) == {email}")
	t.assert_items_equals(index:extract({ email = user.email }), { users[1].email }, ":extract({ email = email }) == {email}")

	-- t.assert_items_equals(index:extract(user), { users[1].email }, ":key(User) == {email}")
	-- t.assert_items_equals(index:extract({user}), { users[1].email }, ":key({ User }) == {email}")
	-- t.assert_items_equals(index:extract({email = user}), { users[1].email }, ":key({ email = User }) == {email}")
end
